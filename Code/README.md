Pré-Requisitos:

* MySQL Workbench

* Mongo Compass Community Edition

* Python 3, com os módulos: MySQL Connector, PyMongo, Requests, XMLToDict, BSON, Collections, Pandas

* Conhecimentos em Python 3, MySQL e Mongo

OBS¹: veja documentação oficial para saber como instalar os bancos MySQL e MongoDB, e as ferramentas MySQL Workbench e MongoDB Compass.

OBS²: Se tiver algum módulo em Python que não tiver instalado na sua máquina, use o comando:

    pip3 install <module>


Compatibilidade: Linux

Organização do Código do Projeto:

     - .mongorc.js: contém uma função usada para medição de tempo de agregações no Mongo. Deve ser copiada para o diretório
                    raiz do usuário.
     
     - CivisAnalysis.sql: responsável por gerar o esquema relacional.
     
     - getVotes.py: obtém as votações de proposições, fazendo requisições HTML GET, salvando a resposta de cada requisição
                    em um arquivo JSON dentro do diretório 'Votacoes'. Para funcionar, ele precisa do 'allMotionsPerYear.json',
                    no diretório raiz, gerado pelo sistema CivisAnalysis e cedido pelo Rodrigo Moni. Possui 'main()'.

     - mainMongo.py: conecta e faz os carregamentos e inserções de dados no banco de dados de documentos do MongoDB. Possui
                     'main()'.
     
     - mainRelational.py: conecta e faz os carregamentos e inserções de dados no banco de dados do MySQL. Possui 'main()'.
     
     - preprocessVoting.py: script necessário antes de executar o 'mainRelational.py', que cria novos CSVs para inserção de
                            dados referentes às votações. Possui 'main()'.
     
     - tableFunctions.py: implementa switch-cases em Python, que são usado para diversos processamentos.
     
     - utilities.py: tem constantes e funções auxiliares.
     
     - voting.py: possui funções específicas para processar as entidades de votação do modelo ER, gerando CSVs.


Ordem de execução:

OBS: A ordem dos 2 primeiros não importa, desde que execute todos os listados abaixo, e execute o 'preprocessVoting.py' antes do 'mainRelational.py':

    1) mergePropositions.sh
    2) getVotes.py
    3) preprocessVoting.py
    4) mainRelational.py OU mainMongo.py


Organização dos Diretórios do Projeto:

    - Arquivos: é onde estão os arquivos CSV, contendo dados a serem carregados nos bancos de dados. Dentro deste diretório,
                tem um subdiretório, chamado 'Proposicoes', com um script em bash ('mergePropositions.sh') para combinar todos
                os CSVs das proposições em um só, para facilitar processamento. Ele deve ser executado antes de rodar o
                programa. Para isso, é necessário primeiro por os CSVs das proposições do site da Câmara dentro de 'Proposicoes'.
    
    - MySQL Load Scripts: contém os scripts SQL para carregamento dos CSVs no banco de dados MySQL.
    
    - Votacoes: contém todos os arquivos de proposições votadas em plenário, obtidos através da execução do 'getVotes.py'.
                Também tem um script que faz a classificação dos arquivos de proposição, para fins de depuração.
