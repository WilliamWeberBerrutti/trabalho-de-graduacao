import utilities as util
import voting as v
import mongoProcessing as mP


"""
  Implementação de 2 grupos de 'cases' de 'switch-cases' em Python 3, usando
  dicionários
"""

switcher_MySQL = {"Votacao": v.processVotacao,
                  "Votacao_has_Deputado": v.processVotacao_has_Deputado,
                  "Votacao_has_Proposicao": v.processVotacao_has_Proposicao
                  }



switcher_Mongo = {"deputados": mP.processDeputados,
                  "deputadosPartidos": mP.processDeputadosPartidos,
                  "frentes": mP.processFrentes,
                  "frentesDeputados": mP.processFrentesDeputados,
                  "legislaturas": mP.processLegislaturas,
                  "partidos": mP.processPartidos,
                  "proposicoes": mP.processProposicoes,
                  "votacoes": mP.processVotacoes
                  }


"""
  Funções que selecionam uma chave de um 'switcher' (como se fosse o 'switch'
  do switch-case)
"""

def processDataFile_MySQL(table):
    
    '''
      Decidi por a abertura dos arquivos de votações aqui para o código ser
      mais polido, para não ficar chamando 3 vezes, ao iniciar cada função
    '''
    if (not util.votingFilesOpen):
        util.openAllMotions()
        
    func = switcher_MySQL.get(table, "Invalid")
    
    if (func is not "Invalid"):
        return func()
    else:
        print("Opção inválida!")
        return


def processDataFile_Mongo(collection, csvfile):
    
    func = switcher_Mongo.get(collection)
    
    '''
      Decidi por as aberturas dos arquivos aqui, para ter menos repetições e
      não precisar chamar a abertura e conversão de arquivos no início de cada
      função
    '''
    if (func is not "Invalid"):
        if (collection == "votacoes"):
            return func()
        
        elif (csvfile.endswith("artidos.csv")):
            csvfile = util.openCSVFile(f"{util.csvFilesDir}/{csvfile}", ",")
        else:
            csvfile = util.openCSVFile(f"{util.csvFilesDir}/{csvfile}", ";")
            
            '''
              Converte o arquivo CSV para JSON, para ser inserido no MongoDB
            '''
            data_json = util.convertToJSON(csvfile)
            
            return func(data_json)
    else:
        print("Opção inválida!")
        return
