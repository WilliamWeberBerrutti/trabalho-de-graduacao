import sys
from timeit import default_timer

import utilities as util


"""
  Função Main.
  
  Algoritmo:
  
    1) Conectar ao BD
    2) Abrir cada arquivo de dados dos Dados Abertos da Câmara, previamente
       baixados da aba ‘Arquivos’ do site ou da aba 'API RESTFul'
    3) Após a abertura de cada arquivo, inserir os dados relevantes no BD
       relacional, inserindo primeiro nas tabelas principais, depois nas
       de histórico
    4) Para as Votações: usar o 'getVotes.py' para fazer requisições HTML GET
       para obter os arquivos, contendo  dados das votações das proposições
    5) Após obter os arquivos das votações, processar um por um, inserindo
       nas tabelas adequadas
"""
def main():
    
    ''' Conecta ao BD '''
    mydb, mycursor = util.connectDataBase()
    
    tableList = util.showTables(mycursor)
    
    '''
      Separa a lista de entidades em 2 listas, para que o carregamento
      de dados seja feito na ordem correta. Os históricos têm "_has_"
    '''
    mainEntitiesTableList = [fn for fn in tableList if "_has_" not in fn]
    historicTableList = [fn for fn in tableList if "_has_" in fn]
    
    '''
      Une as listas, preservando a ordem: entidades primeiro, depois
      históricos
    '''
    orderedEntities = mainEntitiesTableList + historicTableList
    
    # DEBUG
    '''
    util.DEBUG_PrintNamesAndValues(["tableList",
                                    "mainEntitiesTableList",
                                    "historicTableList",
                                    "orderedEntities"],
                                   [tableList,
                                    mainEntitiesTableList,
                                    historicTableList,
                                    orderedEntities])
    '''
    
    # DEBUG
    '''
      Deleção de todas as tabelas do BD antes de inserir os dados, por
      precaução.
    
      OBS: A ordem das entidades está revertida porque precisamos deletar as
      entidades de histórico antes das entidades principais, por causa das
      chaves estrangeiras que as referenciam!
    '''
    '''
    print(f"\n ==== Limpando tabelas ==== \n")
    
    for table in reversed(orderedEntities):
        util.deleteFromDB(mydb, mycursor, table)
    '''
    
    ''' Inserção / carga de dados no BD '''
    
    total_time = 0
    
    for entity in orderedEntities:
        
        print(f"\n\n ==== Processando Tabela: {entity} ==== \n")
        
        start_time = default_timer()
        
        sqlScript = util.openReadFile(f"{util.mysqlScriptsDir}/"
                                      f"{entity}.sql")
        
        ''' Execução do script MySQL de LOAD DATA '''
        util.loadDataIntoDB(mydb, mycursor, sqlScript, False)
        
        elapsed = default_timer() - start_time
            
        print(f"Time: {round(elapsed, 8)} seconds")
        
        total_time += elapsed
    
    
    mydb.close()
    
    print(f"\n\n ==== Connection closed ==== \n")
    
    print(f"Time Elapsed: {round(total_time, 8)} seconds")
    print(f"Rows Inserted: {util.countTotal}")


''' Execução da Main '''
main()
