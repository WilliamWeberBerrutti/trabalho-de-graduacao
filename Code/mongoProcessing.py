import utilities as util
import itertools


"""
  Processa a coleção 'deputados'.
  
  Entradas: data_json = lista de strings, é o conteúdo do arquivo CSV aberto
                        anteriormente e convertido para JSON
  
  Saídas: dictList = lista de dicionários, usada para inserção de dados
"""
def processDeputados(data_json):
    
    ''' Faz com que o 'uri' do Deputado seja o '_id' do Mongo '''
    for elem in data_json:
        elem['_id'] = elem.pop('uri')

    ''' Extrai as colunas de interesse do arquivo '''
    dictList = [{k: elem[k] if k is not '_id' else int(util.getId(elem[k]))
                  for k in ('_id', 'nome', 'ufNascimento',
                            'idLegislaturaInicial', 'idLegislaturaFinal')}
                    for elem in data_json]
    
    return dictList


"""
  Processa a coleção 'deputadosPartidos'.
  
  Entradas: data_json = lista de strings, é o conteúdo do arquivo CSV aberto
                        anteriormente e convertido para JSON
  
  Saídas: dictList = lista de dicionários, usada para inserção de dados
"""
def processDeputadosPartidos(data_json):
    
    ''' Faz com que o 'dados_id' do Deputado seja o '_id' do Mongo '''
    for elem in data_json:
        elem['_id'] = elem.pop('dados_id')
        elem['dados_id_partido'] = elem.pop('dados_uri_partido')

    ''' Extrai as colunas de interesse do arquivo '''
    dictList = [{k: elem[k] if k is not 'dados_id_partido' else \
                                                            util.getId(elem[k])
                  for k in ('_id', 'dados_id_legislatura', 'dados_nome',
                            'dados_sigla_partido', 'dados_sigla_uf',
                            'dados_id_partido')}
                    for elem in data_json]
    
    return dictList


"""
  Processa a coleção 'frentes'.
  
  Entradas: data_json = lista de strings, é o conteúdo do arquivo CSV aberto
                        anteriormente e convertido para JSON
  
  Saídas: dictList = lista de dicionários, usada para inserção de dados
"""
def processFrentes(data_json):
    
    ''' Faz com que o 'id' das Frentes seja o '_id' do Mongo '''
    for elem in data_json:
        elem['_id'] = elem.pop('id')

    ''' Extrai as colunas de interesse do arquivo '''
    dictList = [{k: elem[k]
                  for k in ('_id', 'titulo', 'dataCriacao')}
                    for elem in data_json]
    
    return dictList


"""
  Processa a coleção 'frentesDeputados'.
  
  Entradas: data_json = lista de strings, é o conteúdo do arquivo CSV aberto
                        anteriormente e convertido para JSON
  
  Saídas: dictList = lista de dicionários, usada para inserção de dados
"""
def processFrentesDeputados(data_json):
    
    ''' Renomeação de chaves para facilitar '''
    for elem in data_json:
        
        ''' Renomeia algumas chaves por conveniência '''
        elem['frente_id'] = elem.pop('id')
        
        ''' Substitui chaves que têm pontos, pois não é permitido no Mongo '''
        elem['deputado_id'] = elem.pop('deputado_.id')
        elem['deputado_uriPartido'] = elem.pop('deputado_.uriPartido')
        elem['deputado_nome'] = elem.pop('deputado_.nome')
        elem['deputado_siglaUf'] = elem.pop('deputado_.siglaUf')
        elem['deputado_idLegislatura'] = elem.pop('deputado_.idLegislatura')

    ''' Extrai as colunas de interesse do arquivo '''
    dictList = [{k: elem[k] if k is not 'deputado_.uriPartido' else \
                                                            util.getId(elem[k])
                  for k in ('titulo', 'frente_id',
                            'deputado_uriPartido', 'deputado_nome',
                            'deputado_siglaUf', 'deputado_idLegislatura')}
                    for elem in data_json]
                    
    return dictList


"""
  Processa a coleção 'legislaturas'.
  
  Entradas: data_json = lista de strings, é o conteúdo do arquivo CSV aberto
                        anteriormente e convertido para JSON
  
  Saídas: dictList = lista de dicionários, usada para inserção de dados
"""
def processLegislaturas(data_json):
    
    ''' Faz com que o 'idLegislatura' da Legislatura seja o '_id' do Mongo
    '''
    for elem in data_json:
        elem['_id'] = elem.pop('idLegislatura')

    ''' Extrai as colunas de interesse do arquivo '''
    dictList = [{k: elem[k]
                  for k in ('_id', 'dataInicio', 'dataFim')}
                    for elem in data_json]
    
    return dictList


"""
  Processa a coleção 'partidos'.
  
  Entradas: data_json = lista de strings, é o conteúdo do arquivo CSV aberto
                        anteriormente e convertido para JSON
  
  Saídas: dictList = lista de dicionários, usada para inserção de dados
"""
def processPartidos(data_json):
    
    ''' Faz com que o 'dados_id' do Partido seja o '_id' do Mongo '''
    for elem in data_json:
        elem['_id'] = elem.pop('dados_id')

    ''' Extrai as colunas de interesse do arquivo '''
    dictList = [{k: elem[k]
                  for k in ('_id', 'dados_nome', 'dados_sigla')}
                    for elem in data_json]
    
    return dictList


"""
  Processa a coleção 'proposicoes'.
  
  Entradas: data_json = lista de strings, é o conteúdo do arquivo CSV aberto
                        anteriormente e convertido para JSON
  
  Saídas: dictList = lista de dicionários, usada para inserção de dados
"""
def processProposicoes(data_json):
    
    ''' Faz com que o 'uri' da Proposição seja o '_id' do Mongo '''
    for elem in data_json:
        elem['_id'] = elem.pop('id')

    ''' Extrai as colunas de interesse do arquivo '''
    dictList = [{k: elem[k]
                  for k in ('_id', 'siglaTipo', 'numero', 'ano', 'ementa',
                            'ementaDetalhada', 'keywords', 'dataApresentacao')}
                    for elem in data_json]
    
    return dictList


"""
  Processa a coleção 'votacoes'.
  
  Entradas: data_json = lista de strings, é o conteúdo do arquivo CSV aberto
                        anteriormente e convertido para JSON
  
  Saídas: dictList = lista de dicionários, usada para inserção de dados
"""
def processVotacoes():
    
    util.openAllMotions()
    
    for elem, elem2 in zip(util.jsonDataList, util.idPropList):
        elem.update({'_id' : elem2})
    
    # DEBUG
    '''
    with open("JSONDATALIST.json", "w") as f:
        f.write(str(util.jsonDataList))
    '''
    
    dictList = util.removeDictListDuplicates(util.jsonDataList)
    
    return dictList
