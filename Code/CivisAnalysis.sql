-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema CivisAnalysis
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema CivisAnalysis
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `CivisAnalysis` ;
USE `CivisAnalysis` ;

-- -----------------------------------------------------
-- Table `CivisAnalysis`.`Deputado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CivisAnalysis`.`Deputado` (
  `idDeputado` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(200) NULL,
  `sexo` VARCHAR(1) NULL,
  `siglaUF` VARCHAR(20) NULL,
  PRIMARY KEY (`idDeputado`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CivisAnalysis`.`Legislatura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CivisAnalysis`.`Legislatura` (
  `idLegislatura` INT NOT NULL,
  `dataInicio` DATE NULL,
  `dataFim` DATE NULL,
  PRIMARY KEY (`idLegislatura`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CivisAnalysis`.`Proposicao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CivisAnalysis`.`Proposicao` (
  `idProposicao` INT NOT NULL,
  `siglaTipo` VARCHAR(45) NULL,
  `numero` INT NULL,
  `ano` INT NULL,
  `ementa` MEDIUMTEXT NULL,
  `ementaDetalhada` MEDIUMTEXT NULL,
  `keywords` MEDIUMTEXT NULL,
  `dataApresentacao` DATETIME NULL,
  PRIMARY KEY (`idProposicao`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CivisAnalysis`.`Votacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CivisAnalysis`.`Votacao` (
  `idVotacao` INT NOT NULL AUTO_INCREMENT,
  `codSessao` INT NOT NULL,
  `resumo` VARCHAR(25000) NULL,
  `objVotacao` VARCHAR(25000) NULL,
  `data` DATE NULL,
  `hora` TIME NOT NULL,
  PRIMARY KEY (`idVotacao`, `codSessao`, `hora`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CivisAnalysis`.`Partido`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CivisAnalysis`.`Partido` (
  `idPartido` INT NOT NULL,
  `nome` VARCHAR(200) NULL,
  `siglaPartido` VARCHAR(200) NULL,
  PRIMARY KEY (`idPartido`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CivisAnalysis`.`Legislatura_has_Deputado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CivisAnalysis`.`Legislatura_has_Deputado` (
  `Deputado_idDeputado` INT NOT NULL,
  `Legislatura_idLegislaturaInicial` INT NOT NULL,
  `Legislatura_idLegislaturaFinal` INT NOT NULL,
  PRIMARY KEY (`Deputado_idDeputado`, `Legislatura_idLegislaturaInicial`, `Legislatura_idLegislaturaFinal`),
  INDEX `fk_Legislatura_has_Deputado_Deputado1_idx` (`Deputado_idDeputado` ASC),
  INDEX `fk_Legislatura_has_Deputado_Legislatura2_idx` (`Legislatura_idLegislaturaFinal` ASC),
  CONSTRAINT `fk_Legislatura_has_Deputado_Deputado1`
    FOREIGN KEY (`Deputado_idDeputado`)
    REFERENCES `CivisAnalysis`.`Deputado` (`idDeputado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Legislatura_has_Deputado_Legislatura1`
    FOREIGN KEY (`Legislatura_idLegislaturaInicial`)
    REFERENCES `CivisAnalysis`.`Legislatura` (`idLegislatura`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Legislatura_has_Deputado_Legislatura2`
    FOREIGN KEY (`Legislatura_idLegislaturaFinal`)
    REFERENCES `CivisAnalysis`.`Legislatura` (`idLegislatura`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CivisAnalysis`.`Frente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CivisAnalysis`.`Frente` (
  `idFrente` INT NOT NULL,
  `titulo` VARCHAR(200) NULL,
  `dataCriacao` DATE NULL,
  PRIMARY KEY (`idFrente`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CivisAnalysis`.`Frente_has_Deputado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CivisAnalysis`.`Frente_has_Deputado` (
  `Frente_idFrente` INT NOT NULL,
  `Deputado_idDeputado` INT NOT NULL,
  `Titulo` VARCHAR(45) NULL,
  PRIMARY KEY (`Frente_idFrente`, `Deputado_idDeputado`),
  INDEX `fk_Frente_has_Deputado_Deputado1_idx` (`Deputado_idDeputado` ASC),
  INDEX `fk_Frente_has_Deputado_Frente1_idx` (`Frente_idFrente` ASC),
  CONSTRAINT `fk_Frente_has_Deputado_Frente1`
    FOREIGN KEY (`Frente_idFrente`)
    REFERENCES `CivisAnalysis`.`Frente` (`idFrente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Frente_has_Deputado_Deputado1`
    FOREIGN KEY (`Deputado_idDeputado`)
    REFERENCES `CivisAnalysis`.`Deputado` (`idDeputado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CivisAnalysis`.`Legislatura_has_Frente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CivisAnalysis`.`Legislatura_has_Frente` (
  `Legislatura_idLegislatura` INT NOT NULL,
  `Frente_idFrente` INT NOT NULL,
  PRIMARY KEY (`Legislatura_idLegislatura`, `Frente_idFrente`),
  INDEX `fk_Legislatura_has_Frente_Frente1_idx` (`Frente_idFrente` ASC),
  INDEX `fk_Legislatura_has_Frente_Legislatura1_idx` (`Legislatura_idLegislatura` ASC),
  CONSTRAINT `fk_Legislatura_has_Frente_Legislatura1`
    FOREIGN KEY (`Legislatura_idLegislatura`)
    REFERENCES `CivisAnalysis`.`Legislatura` (`idLegislatura`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Legislatura_has_Frente_Frente1`
    FOREIGN KEY (`Frente_idFrente`)
    REFERENCES `CivisAnalysis`.`Frente` (`idFrente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CivisAnalysis`.`Proposicao_has_Deputado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CivisAnalysis`.`Proposicao_has_Deputado` (
  `Proposicao_idProposicao` INT NOT NULL,
  `Deputado_idDeputado` INT NOT NULL,
  PRIMARY KEY (`Proposicao_idProposicao`, `Deputado_idDeputado`),
  INDEX `fk_Proposicao_has_Deputado_Deputado1_idx` (`Deputado_idDeputado` ASC),
  INDEX `fk_Proposicao_has_Deputado_Proposicao1_idx` (`Proposicao_idProposicao` ASC),
  CONSTRAINT `fk_Proposicao_has_Deputado_Proposicao1`
    FOREIGN KEY (`Proposicao_idProposicao`)
    REFERENCES `CivisAnalysis`.`Proposicao` (`idProposicao`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Proposicao_has_Deputado_Deputado1`
    FOREIGN KEY (`Deputado_idDeputado`)
    REFERENCES `CivisAnalysis`.`Deputado` (`idDeputado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CivisAnalysis`.`Deputado_has_Partido`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CivisAnalysis`.`Deputado_has_Partido` (
  `Deputado_idDeputado` INT NOT NULL,
  `Partido_idPartido` INT NOT NULL,
  PRIMARY KEY (`Deputado_idDeputado`, `Partido_idPartido`),
  INDEX `fk_Deputado_has_Partido_Partido1_idx` (`Partido_idPartido` ASC),
  INDEX `fk_Deputado_has_Partido_Deputado1_idx` (`Deputado_idDeputado` ASC),
  CONSTRAINT `fk_Deputado_has_Partido_Deputado1`
    FOREIGN KEY (`Deputado_idDeputado`)
    REFERENCES `CivisAnalysis`.`Deputado` (`idDeputado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Deputado_has_Partido_Partido1`
    FOREIGN KEY (`Partido_idPartido`)
    REFERENCES `CivisAnalysis`.`Partido` (`idPartido`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CivisAnalysis`.`Votacao_has_Deputado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CivisAnalysis`.`Votacao_has_Deputado` (
  `Votacao_idVotacao` INT NOT NULL,
  `Votacao_codSessao` INT NOT NULL,
  `Votacao_hora` TIME NOT NULL,
  `Deputado_idDeputado` INT NOT NULL,
  `voto` VARCHAR(20) NULL,
  PRIMARY KEY (`Votacao_idVotacao`, `Votacao_codSessao`, `Votacao_hora`, `Deputado_idDeputado`),
  INDEX `fk_Votacao_has_Deputado_Deputado1_idx` (`Deputado_idDeputado` ASC),
  INDEX `fk_Votacao_has_Deputado_Votacao1_idx` (`Votacao_idVotacao` ASC, `Votacao_codSessao` ASC, `Votacao_hora` ASC),
  CONSTRAINT `fk_Votacao_has_Deputado_Votacao1`
    FOREIGN KEY (`Votacao_idVotacao` , `Votacao_codSessao` , `Votacao_hora`)
    REFERENCES `CivisAnalysis`.`Votacao` (`idVotacao` , `codSessao` , `hora`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Votacao_has_Deputado_Deputado1`
    FOREIGN KEY (`Deputado_idDeputado`)
    REFERENCES `CivisAnalysis`.`Deputado` (`idDeputado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `CivisAnalysis`.`Votacao_has_Proposicao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CivisAnalysis`.`Votacao_has_Proposicao` (
  `Votacao_idVotacao` INT NOT NULL,
  `Votacao_codSessao` INT NOT NULL,
  `Votacao_hora` TIME NOT NULL,
  `Proposicao_idProposicao` INT NOT NULL,
  PRIMARY KEY (`Votacao_idVotacao`, `Votacao_codSessao`, `Votacao_hora`, `Proposicao_idProposicao`),
  INDEX `fk_Votacao_has_Proposicao_Proposicao1_idx` (`Proposicao_idProposicao` ASC),
  INDEX `fk_Votacao_has_Proposicao_Votacao1_idx` (`Votacao_idVotacao` ASC, `Votacao_codSessao` ASC, `Votacao_hora` ASC),
  CONSTRAINT `fk_Votacao_has_Proposicao_Votacao1`
    FOREIGN KEY (`Votacao_idVotacao` , `Votacao_codSessao` , `Votacao_hora`)
    REFERENCES `CivisAnalysis`.`Votacao` (`idVotacao` , `codSessao` , `hora`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Votacao_has_Proposicao_Proposicao1`
    FOREIGN KEY (`Proposicao_idProposicao`)
    REFERENCES `CivisAnalysis`.`Proposicao` (`idProposicao`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
