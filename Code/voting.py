import datetime

import utilities as util


"""
  Cria 'votacoes.csv' (baseada nos arquivos de votações) para facilitar
  processamento da entidade 'Votacao' pelo 'mainRelational.py'
  
  Entradas: n/a
  
  Saídas: n/a
"""
def processVotacao():
    
    insertList = [('idVotacao', 'codSessao', 'resumo', 'objVotacao',
                   'data', 'hora')]
    insertStr = ""
    
    auxList = []    # lista auxiliar para eliminar duplicatas
    
    if (not util.exists(f"{util.csvFilesDir}/votacoes.csv")):
        
        for idVotacao, jsondata in zip(util.idVotacaoList, util.jsonDataList):
            
            if (type(jsondata["proposicao"]["Votacoes"]["Votacao"]) == list):
                for elem in jsondata["proposicao"]["Votacoes"]["Votacao"]:
                    
                    codSessao = elem["@codSessao"]
                    resumo = elem["@Resumo"]
                    objVotacao = elem["@ObjVotacao"]
                    data = elem["@Data"]
                    hora = elem["@Hora"]
                    
                    auxList.append((idVotacao,
                                    codSessao,
                                    hora))
                    
                    insertList.append((idVotacao,
                                       codSessao,
                                       resumo,
                                       objVotacao,
                                       datetime.datetime.strptime(data,
                                         '%d/%m/%Y').date().isoformat(),
                                       hora))
                                           
            
            elif (type(jsondata["proposicao"]["Votacoes"]["Votacao"]) == dict):
                codSessao = jsondata["proposicao"]["Votacoes"]["Votacao"]\
                            .get("@codSessao")
                resumo = jsondata["proposicao"]["Votacoes"]["Votacao"]\
                         .get("@Resumo")
                objVotacao = jsondata["proposicao"]["Votacoes"]["Votacao"]\
                             .get("@ObjVotacao")
                data = jsondata["proposicao"]["Votacoes"]["Votacao"]\
                       .get("@Data")
                data = datetime.datetime.strptime(data, '%d/%m/%Y').date()\
                       .isoformat()
                hora = jsondata["proposicao"]["Votacoes"]["Votacao"]\
                       .get("@Hora")
                
                auxList.append((idVotacao, codSessao, resumo, objVotacao, data,
                                hora))
                
                insertList.append((idVotacao, codSessao, resumo, objVotacao,
                                   data, hora))
        
        
        # DEBUG
        '''
        with open("Votacao_Log.txt", 'w') as f:
            f.write(str(insertList))
        '''
        
        ''' Remove duplicatas '''
        util.removeDuplicates(auxList, insertList)
        
        util.writeCSVFile(f"{util.csvFilesDir}/votacoes.csv", insertList)


"""
  Cria 'votacoesDeputados.csv' (baseada nos arquivos de votações) para
  facilitar processamento da entidade 'Votacao_has_Deputado' pelo
  'mainRelational.py'
  
  Entradas: n/a
  
  Saídas: n/a
"""
def processVotacao_has_Deputado():
    
    insertList = [('idVotacao', 'codSessao', 'hora', 'idDeputado', 'voto')]
    insertStr = ""
    
    auxList = []    # lista auxiliar para eliminar duplicatas
    
    if (not util.exists(f"{util.csvFilesDir}/votacoesDeputados.csv")):
        
        for jsondata, idVotacao in zip(util.jsonDataList, util.idVotacaoList):
            
            lenVotacao = len(jsondata["proposicao"]["Votacoes"]["Votacao"])
            
            if (type(jsondata["proposicao"]["Votacoes"]["Votacao"]) == list):
                for idx in range(lenVotacao):
                    
                    lenVotosDeputados = len(jsondata["proposicao"]["Votacoes"]\
                                                    ["Votacao"][idx]["votos"]\
                                                    ["Deputado"])
                    
                    codSessao = jsondata["proposicao"]["Votacoes"]["Votacao"]\
                                        [idx]["@codSessao"]
                    hora = jsondata["proposicao"]["Votacoes"]["Votacao"][idx]\
                                   ["@Hora"]
                    
                    for elem in jsondata["proposicao"]["Votacoes"]["Votacao"]\
                                        [idx]["votos"]["Deputado"]:
                        
                        idDeputado = elem["@ideCadastro"]
                        voto = elem["@Voto"]#.rstrip(" ")
                        '''
                          Comentei o 'rstrip', para ficar o mais parecido
                          possível com o Mongo, para não ter que processar isso
                          nele também, para agilizar / simplificar
                        '''
                        
                        '''
                          É necessário fazer isto porque tem votos que não
                          tem registrado o ID do Deputado, que é PK e FK nesta
                          tabela
                        '''
                        if (idVotacao and codSessao and idDeputado and voto):
                            auxList.append((idVotacao, codSessao, hora,
                                            idDeputado))
                            
                            insertList.append((idVotacao, codSessao, hora,
                                               idDeputado, voto))
                                
            
            elif (type(jsondata["proposicao"]["Votacoes"]["Votacao"]) == dict):
                for idx in range(len(jsondata["proposicao"]["Votacoes"]\
                                     ["Votacao"]["votos"]["Deputado"])):
                    idDeputado = jsondata["proposicao"]["Votacoes"]["Votacao"]\
                                         ["votos"]["Deputado"][idx]\
                                         .get("@ideCadastro")
                    codSessao = jsondata["proposicao"]["Votacoes"]["Votacao"]\
                                        .get("@codSessao")
                    hora = jsondata["proposicao"]["Votacoes"]["Votacao"]\
                                   .get("@Hora")
                    voto = jsondata["proposicao"]["Votacoes"]["Votacao"]\
                                   ["votos"]["Deputado"][idx].get("@Voto")#\
                                   #.rstrip(" ")
                    '''
                      Comentei o 'rstrip', para ficar o mais parecido
                      possível com o Mongo, para não ter que processar isso
                      nele também, para agilizar / simplificar
                    '''
                    
                    '''
                      É necessário fazer isto porque tem votos que não
                      tem registrado o ID do Deputado, que é PK e FK nesta
                      tabela
                    '''
                    if (idVotacao and codSessao and idDeputado and voto):
                        auxList.append((idVotacao, codSessao, hora,
                                        idDeputado))
                        
                        insertList.append((idVotacao, codSessao, hora,
                                           idDeputado, voto))
        
        
        # DEBUG
        '''
        with open("Votacao_has_Deputado_Log.txt", 'w') as f:
            f.write(str(insertList))
        '''
        
        ''' Remove duplicatas e Deputados que não existem na base de dados '''
        util.removeDuplicates(auxList, insertList)
        
        util.writeCSVFile(f"{util.csvFilesDir}/votacoesDeputados.csv",
                          insertList)


"""
  Cria 'votacoesProposicoes.csv' (baseada nos arquivos de votações) para
  facilitar processamento da entidade 'Votacao_has_Proposicao' pelo
  'mainRelational.py'
  
  Entradas: n/a
  
  Saídas: n/a
"""
def processVotacao_has_Proposicao():
    
    insertList = [('idVotacao', 'codSessao', 'hora', 'idProp')]
    insertStr = ""
    
    auxList = []    # lista auxiliar para eliminar duplicatas
    
    if (not util.exists(f"{util.csvFilesDir}/votacoesProposicoes.csv")):
        
        for jsondata, idVotacao, idProp in zip(util.jsonDataList,
                                               util.idVotacaoList,
                                               util.idPropList):
            
            lenVotacao = len(jsondata["proposicao"]["Votacoes"]["Votacao"])
            
            if (type(jsondata["proposicao"]["Votacoes"]["Votacao"]) == list):
                for idx in range(lenVotacao):
                    
                    codSessao = jsondata["proposicao"]["Votacoes"]["Votacao"]\
                                        [idx]["@codSessao"]
                    hora = jsondata["proposicao"]["Votacoes"]["Votacao"][idx]\
                                   ["@Hora"]
                    
                    auxList.append((idVotacao, codSessao, hora, idProp))
                    
                    insertList.append((idVotacao, codSessao, hora, idProp))
                    
            
            elif (type(jsondata["proposicao"]["Votacoes"]["Votacao"]) == dict):
                codSessao = jsondata["proposicao"]["Votacoes"]["Votacao"]\
                                    .get("@codSessao")
                hora = jsondata["proposicao"]["Votacoes"]["Votacao"]\
                       .get("@Hora")
                
                auxList.append((idVotacao, codSessao, hora, idProp))
                
                insertList.append((idVotacao, codSessao, hora, idProp))
    
    
        # DEBUG            
        '''
        with open("Votacao_has_Proposicao_Log.txt", 'w') as f:
            f.write(str(insertList))
        '''
        
        ''' Remove duplicatas '''   
        util.removeDuplicates(auxList, insertList)
        
        util.writeCSVFile(f"{util.csvFilesDir}/votacoesProposicoes.csv",
                          insertList)
