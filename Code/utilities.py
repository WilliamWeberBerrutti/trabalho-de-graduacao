"""
  Este arquivo tem funções auxiliares e constantes
"""

from ast import literal_eval
import csv
import sys
from json import load, loads, dump
from os import listdir, path
from os.path import isfile, join, exists

from bson import json_util
from collections import Counter
from pandas import read_csv
from pymongo import MongoClient
import mysql.connector


""" Estados do programa """

SUCCESS = True      # função executou corretamente
ERROR = False       # função não executou corretamente
FOUND = True        # um elemento foi encontrado
FAILURE = False     # erro grave no programa
INVALID = False     # informação inválida


""" Estruturas auxiliares """

mysqlScriptsDir = "MySQL Load Scripts"
csvFilesDir = "Arquivos"
jsonFilesDir = "Votacoes"
dataBaseName = "CivisAnalysis"

votingFilesOpen = False # booleano que indica se os arquivos de votações
                        # já foram abertos

motionsFile = []
propIdList = []

idVotacaoList = []
idPropList = []
jsonDataList = []

countTotal = 0  # responsável por contar quantos registros / documentos foram
                # inseridos nos bancos de dados (MySQL e Mongo)


""" Cria um dialeto para o CSV """
'''
  OBS: Tem que ser com vírgula para os scripts de importação do Mongo
  funcionarem!
'''
csv.register_dialect(dataBaseName, delimiter = ',', lineterminator = '\n')


"""
  Responsável por conectar ao banco de dados não-relacional MongoDB.
  
  Entradas: n/a
  
  Saídas: db = objeto banco de dados, representa um seletor de banco de dados
          client = objeto cliente, representa o cliente conectado ao banco de
                   dados
"""
def connectMongoDB():
    
    client = MongoClient('localhost', 27017)
    
    db = client[dataBaseName]
    
    if (client and db):
        
        print(f"Connection: SUCCESS!")
        print(f"Connected to DataBase: {db}.")
        
        return db, client


"""
  Pega o os identificadores de um arquivo de votação. Esses identificadores
  estão no nome do arquivo.
  
  Entradas: name = string, é o nome do arquivo
           
  Saídas: finalList = lista de strings, contém os identificadores de um
                      arquivo de votação, que são: ID da Votação, ID da
                      Proposição, Sigla da Proposição, Número da Proposição
                      e Ano de Votação da Proposição.
"""
def getVotingIdentifiers(name):
    
    finalList = []
    
    tempStr = name.replace(".json", "")
    finalList = tempStr.split(" ")
    finalList.remove("Prop")
    
    return finalList


"""
  Pega o ID, se estiver numa URI, separando em substrings, e retornando a
  última substring.
  
  Entradas: row = string, contém o texto a ser processado
           
  Saídas: int, é o número identificador
"""
def getId(row):
    return row.split("/")[-1]


"""
  Faz a inserção de documentos no banco de dados de documentos Mongo.
  
  Entradas: collection = string, é o nome da coleção a inserir os documentos
            dictList = lista de dicionários, é o conteúdo a inserir no banco
                       de dados
           
  Saídas: n/a
"""
def insertDocs(collection, dictList):
    
    global countTotal
    
    insertLen = len(collection.insert(dictList))
        
    if (insertLen):
        countTotal += insertLen
        print(f"Insertion: SUCCESS!")
        print(f"Docs: {insertLen} added")
    else:
        print(f"Insertion: ERROR!")


"""
  Faz um INSERT INTO no banco de dados relacional MySQL.
  
  Entradas: mydb = objeto conexão, representa a conexão feita no DB
            mycursor = objeto cursor, permite a execução de queries no DB
            tablename = string, nome da tabela a serem inseridos os dados
            fields = string, campos a serem inseridos na tabela 'tablename'
            printWarnings = booleano, indica se é para imprimir avisos
           
  Saídas: n/a
"""
# NÃO É MAIS NECESSÁRIA, MAS VOU DEIXAR
def insertIntoDB(mydb, mycursor, tablename, fields, printWarnings):
    
    mycursor.execute(f"INSERT INTO {tablename} VALUES {fields};")

    mydb.commit()
    
    rows = mycursor.rowcount
    
    if (printWarnings):
        warnings = mycursor.fetchwarnings()
        
        if (warnings):
            print(*warnings, sep="\n")
    
    if (rows):
        print(f"Insertion: SUCCESS!")
        print(f"Rows: {rows} added")
        return rows
    else:
        print(f"Insertion: ERROR!")
        return ERROR


"""
  Faz um LOAD DATA LOCAL FILE no banco de dados relacional MySQL.
  
  Entradas: mydb = objeto conexão, representa a conexão feita no DB
            mycursor = objeto cursor, permite a execução de queries no DB
            tablename = string, nome da tabela a serem inseridos os dados
            fields = string, campos a serem inseridos na tabela 'tablename'
            printWarnings = booleano, indica se é para imprimir avisos
            
  Saídas: n/a
"""
def loadDataIntoDB(mydb, mycursor, sqlScript, printWarnings):
    
    global countTotal
    
    mycursor.execute(sqlScript)

    mydb.commit()
    
    rows = mycursor.rowcount
    
    if (printWarnings):
        warnings = mycursor.fetchwarnings()
        
        if (warnings):
            print(*warnings, sep="\n")
    
    if (rows):
        countTotal += rows
        print(f"Load: SUCCESS!")
        print(f"Rows: {rows} added")
    else:
        print(f"Load: ERROR!")


"""
  Faz um SELECT no banco de dados relacional MySQL.
  
  Entradas: mydb = objeto conexão, representa a conexão feita no DB
            mycursor = objeto cursor, permite a execução de queries no DB
            tablename = string, nome da tabela a serem selecionados os dados
           
  Saídas: n/a
"""
# NÃO É MAIS NECESSÁRIA, MAS VOU DEIXAR
def selectFromDB(mydb, mycursor, tablename):
    
    mycursor.execute(f"SELECT * FROM {tablename};")

    myresult = mycursor.fetchall()
    
    for elem in myresult:
        print(elem)
        
    print("")


"""
  Faz um DELETE FROM no banco de dados relacional MySQL.
  
  Entradas: mydb = objeto conexão, representa a conexão feita no DB
            mycursor = objeto cursor, permite a execução de queries no DB
            tablename = string, nome da tabela a serem limpos os dados
           
  Saídas: n/a
"""
def deleteFromDB(mydb, mycursor, tablename):
    
    mycursor.execute(f"DELETE FROM {tablename};")
    
    mydb.commit()
    
    print(f"{tablename} DELETED.\n")


"""
  Lista todas as tabelas do BD relacional MySQL.
  
  Entradas: mycursor = objeto cursor, permite a execução de queries no DB
  
  Saídas: myresult = lista de strings, contendo os nomes das tabelas do BD
"""
def showTables(mycursor):
    mycursor.execute("SHOW Tables;")

    myresult = mycursor.fetchall()
    
    ''' Remove os nomes das tabelas das tuplas '''
    myresult = [tablename for (tablename,) in myresult]
    
    return myresult


"""
  Responsável por conectar ao banco de dados relacional MySQL.
  
  Entradas: type = string, é o tipo de BD a se conectar (relacional ou
                   documentos)
  
  Saídas: mydb = objeto conexão, representa a conexão feita no DB
          mycursor = objeto cursor, permite a execução de queries no DB
"""
def connectDataBase():
    
    mydb = mysql.connector.connect(host = 'localhost',
                                   database = dataBaseName,
                                   user = 'debian-sys-maint',
                                   password = 'ORw0BOjVZ0mzQ2ym',
                                   allow_local_infile = True)
    
    mydb.get_warnings = True
    
    mycursor = mydb.cursor()
    
    if (mydb.is_connected()):
        
        mycursor.execute("SELECT DATABASE();")
        
        myresult = mycursor.fetchone()
        
        print(f"Connection: SUCCESS!")
        print(f"Connected to DataBase: {myresult[0]}.")
        
        return mydb, mycursor


"""
  Faz um processamento de texto para obter a sigla, o número e o ano de
  uma Proposição.
  
  Entradas: name = string, é o nome da Proposição. Pode conter nomes de 2
                   Proposições, separadas por '=>'
           
  Saídas: finalList = lista de strings, onde cada entrada contém uma parte
                      do nome de uma Proposição (sigla, número e ano,
                      respectivamente)
"""
def getActualPropositionIdentifiers(name):
    
    auxList = []

    tempList = name.split(" ")
    auxList = tempList[1].split("/")
    auxList.append(tempList[0])
    
    ''' Vamos botar seus identificadores em ordem '''
    auxList[0], auxList[1], auxList[2] = auxList[2], auxList[0], auxList[1]

    return auxList


"""
  Lista todos os arquivos de um diretório, classificando em ordem alfabética.
  
  Entradas: path = string, representa o caminho de diretório
  
  Saídas: lista de strings, contendo os nomes dos arquivos
"""
def listFiles(path, extension):
    return sorted([f for f in listdir(path)
            if (isfile(join(path, f)) and f.endswith(extension))])


"""
  Remove os votos cujas IDs não correspondem a IDs dos Deputados da Câmara
  
  Entradas: jsonDataList = lista de strings, representa uma lista de conteúdos
                           de todos os arquivos JSON abertos
  
  Saídas: jsonDataList = lista de strings, representa uma lista de conteúdos
                         de todos os arquivos JSON abertos
"""
def removeInvalidIDs(jsonDataList):
    
    ''' Abre o arquivo CSV dos Deputados '''
    csvfile = openCSVFile(f"{csvFilesDir}/deputados.csv", ";")

    ''' Obtém apenas a coluna 'uri' '''
    uriDepList = csvfile['uri'].tolist()

    ''' Obtém os idDeputados '''
    idDepList = [getId(elem) for elem in uriDepList]
    
    for jsondata in jsonDataList:
        if (type(jsondata['proposicao']['Votacoes']['Votacao']) == list):
            
            l = [[dep for dep in votacao['votos']['Deputado']
                if (dep['@ideCadastro'] in idDepList)]
                  for votacao, idx in zip(jsondata['proposicao']['Votacoes']
                                                  ['Votacao'] ,
                                          range(len(jsondata['proposicao']
                                                            ['Votacoes']
                                                            ['Votacao'])))]
                                                            
            '''
              Precisa disto para substituir a lista interna de votos,
              removendo as entradas cujos IdDeps são inválidos!
            '''
            for idx in range(len(l)):
                jsondata['proposicao']['Votacoes']['Votacao'][idx]['votos']\
                        ['Deputado'] = l[idx]
            
            
        elif (type(jsondata['proposicao']['Votacoes']['Votacao']) == dict):
            
            l = [elem for elem in jsondata['proposicao']['Votacoes']
                                           ['Votacao']['votos']['Deputado']
                    if (elem['@ideCadastro'] in idDepList)]
            
            jsondata['proposicao']['Votacoes']['Votacao']['votos']\
                    ['Deputado'] = l
    
    return jsonDataList


"""
  Função que verifica se tem um ID de Deputado que não está na base de
  dados. Pode ser usada para verificação. Não é necessário executar
  rotineiramente.
  
  Entradas: mycursor = objeto cursor, permite a execução de queries no DB
            insertList = lista de strings, representa a tupla de valores
                         de inserção do comando INSERT
  
  Saídas: n/a
  
  OBS: Gera um arquivo, contendo os IDs inválidos dos Deputados!
"""
def preprocessDeputies(mycursor, insertList):
    
    invalidIDFiles = []
    
    for idDep in insertList:
        mycursor.execute(f"""SELECT idDeputado
                             FROM Deputado D
                             WHERE {idDep[3]} = D.idDeputado;""")

        result = mycursor.fetchall()
        
        ''' Se não encontrou, deleta as entradas das listas '''
        if (not result):
            
            # DEBUG
            '''
            print(f"""Deputado {idDep[3]} não encontrado no BD! Salvando 
                  f"""seu ID no arquivo e deletando!""")
            '''
            
            invalidIDFiles.append(idDep[3])
            
            delIdx = insertList.index(idDep)
            
            del insertList[delIdx]
    
    invalidIDFiles = removeListDuplicates(invalidIDFiles)
    
    # DEBUG
    '''
    with open("Not valid IdDeps.txt", 'w') as f:
        for elem in invalidIDFiles:
            f.writelines(f"{elem}\n")
    '''


"""
  Remove entradas inválidas de proposições que não existem na tabela de
  Proposições. É necessário porque os dados das votações são corrompidos!
  
  Entradas: mydb = objeto conexão, representa a conexão feita no DB
            mycursor = objeto cursor, permite a execução de queries no DB
            jsonFiles = lista de strings, representa a lista dos nomes dos
                        arquivos JSON
            idVotacaoList = lista de inteiros, é uma lista de ID das votações
            idPropList = lista de inteiros, é uma lista de ID das proposições
            jsonDataList = lista de strings, representa o conteúdo da lista
                           de arquivos JSON

  Saídas: n/a
  
  OBS: Gera um arquivo, contendo os IDs inválidos das Proposições!
"""
# NÃO É MAIS NECESSÁRIA, MAS VOU DEIXAR
def preprocessPropositions(mydb, mycursor, jsonFiles, idVotacaoList,
                           idPropList, jsonDataList):
    
    invalidIDFiles = []
    
    for idProp in idPropList:
        mycursor.execute(f"""SELECT idProposicao
                             FROM Proposicao P
                             WHERE {idProp} = P.idProposicao;""")

        result = mycursor.fetchall()
        
        ''' Se não encontrou, deleta as entradas das listas '''
        if (not result):
            
            # DEBUG
            '''
            print(f"""Prop {idProp} não encontrada no BD! Salvando sua"""
                  f"""ID no arquivo e deletando!""")
            '''
            
            invalidIDFiles.append(idProp)
            
            delIdx = idPropList.index(idProp)
            
            del jsonFiles[delIdx]
            del idVotacaoList[delIdx]
            del idPropList[delIdx]
            del jsonDataList[delIdx]
    
    invalidIDFiles = removeListDuplicates(invalidIDFiles)
    
    # DEBUG
    '''
    with open("Not valid IdProps.txt", 'w') as f:
        for elem in invalidIDFiles:
            f.writelines(f"{elem}\n")
    '''

"""
  Remove todos os elementos duplicados de duas listas.
  
  Entradas: myList = lista de elementos, é a lista auxiliar para verificar
                     duplicatas
            myList2 = lista de elementos, é a lista principal para remover
                      os elementos
  
  Saídas: n/a
"""
def removeDuplicates(myList, myList2):
    
    dupDict = Counter(myList)

    for key, value in dupDict.items():
        
        auxValue = value
        
        while (auxValue > 1):
            dupIdx = myList.index(key)
            
            # DEBUG
            '''
            print(f"Key: {key}")
            print(f"Value: {value}")
            print(f"dupIdx: {dupIdx}")
            print(f"\nRemover: {myList[dupIdx]}\n")
            '''
            
            '''
              Precisa remover dos 2 para não acontecer de ter acesso fora
              da lista (por ex, o index() retornaria um índice da myList
              que NÃO EXISTE em myList2!
            '''
            del myList[dupIdx]
            del myList2[dupIdx]
            
            auxValue -= 1


"""
  Remove elementos duplicados de uma lista.
  
  Entradas: myList = lista de elementos, contendo repetições
  
  Saídas: lista de elementos sem repetições
"""
def removeListDuplicates(myList):
    return list(dict.fromkeys(myList))


"""
  Remove elementos duplicados de uma lista de dicionários, que é "unhashable".
  
  Entradas: myDictList = lista de dicionários, contendo repetições
  
  Saídas: lista de dicionários, sem as repetições
"""
def removeDictListDuplicates(myDictList):
    return [item for n, item in enumerate(myDictList)
            if item not in myDictList[n + 1:]]


"""
  Substitui os placeholders dos scripts MySQL pelos valores adequados.
  
  Entradas: sqlScript = string, representa o conteúdo do arquivo de script
                        MySQL aberto
            tablename = string, é o nome da tabela a ser inserida os dados
            sepname = string, é o separador do arquivo CSV correspondente
  
  Saídas: sqlScript = string, corresponde ao script SQL devidamente corrigido
                      para execução no BD
"""
# NÃO É MAIS NECESSÁRIA, MAS VOU DEIXAR
def replaceScriptPlaceholders(sqlScript, datafilename, tablename):
    
    sqlScript = sqlScript.replace("<filename>", f"'{csvFilesDir}/'"
                                                f"'{datafilename}'")
    sqlScript = sqlScript.replace("<tablename>", tablename)
    
    return sqlScript


"""
  Remove CSVs desnecessários na lista de arquivos 'csvDataFiles', para
  simplificar processamento.
  
  Entradas: csvDataFiles = lista de strings, é a lista de nomes dos arquivos
                           CSV
  
  Saídas: newList = lista de strings, é a lista de nomes filtrada
"""
def removeCSVsForMongo(csvDataFiles):
    newList = [elem for elem in csvDataFiles
                    if elem != "votacoesDeputados.csv" and
                       elem != "votacoesProposicoes.csv"]
    return newList


"""
  Formata um script de inserção, removendo caracteres que causariam erros
  de sintaxe.
  
  Entradas: script = string, é o texto do INSERT INTO
  
  Saídas: string, é o texto do INSERT INTO devidamente formatado
"""
# NÃO É MAIS NECESSÁRIA, MAS VOU DEIXAR
def formatInsertScript(script):
    return script.replace('((', '(', 1).replace('))', ')', 1).replace(
                          '),)', ')', 1).replace('))', ')', 1)


"""
  Converte o conteúdo de um arquivo CSV para JSON.
  
  Entradas: csvfile = string, conteúdo do arquivo de dados CSV
            
  Saídas: lista, conteúdo do arquivo CSV convertido para JSON
"""
def convertToJSON(csvfile):
    return json_util.loads(csvfile.to_json(orient='records'))


"""
  Abre um arquivo JSON de dados.
  
  Entradas: filename = string, nome e formato do arquivo de dados
            
  Saídas: json_data = lista de dados lida do arquivo
"""
def openJSONFile(filename):
    
    with open(filename) as jsonfile:
        jsondata = load(jsonfile)
        
    return jsondata


"""
  Abre um arquivo CSV de dados.
  
  Entradas: filename = string, nome e formato do arquivo de dados
            delim = string, delimitador do arquivo CSV
            
  Saídas: objeto DataFrame, contém o arquivo lido
"""
def openCSVFile(filename, delim):
    return read_csv(filename, low_memory = False, sep = delim).fillna("")


"""
  Escreve um arquivo CSV no disco.
  
  Entradas: filename = string, é nome do arquivo
            content = lista de strings, é o conteúdo do arquivo
            delim = string, delimitador do CSV
            lineterm = string, caractere de terminação de linhas
  
  Saídas: n/a
"""
def writeCSVFile(filename, content):
    
    with open(filename, 'w') as f:
        writer = csv.writer(f, dialect = dataBaseName)
        writer.writerows(content)


"""
  Abre um arquivo qualquer de dados, lendo os dados como string.
  
  Entradas: filename = string, nome e formato do arquivo de dados
            
  Saídas: filedata = string, representa o arquivo lido
"""
def openReadFile(filename):
    
    with open(filename) as f:
        filedata = f.read()
        
    return filedata


"""
  Abre um arquivo qualquer de dados, lendo os dados e armazenando-os como uma
  lista de strings.
  
  Entradas: filename = string, nome e formato do arquivo de dados
            datatype = string, corresponde ao tipo de dados para converter cada
                       linha do arquivo
            
  Saídas: filedata = lista de strings, representa o arquivo lido
"""
def openReadFileAsList(filename, datatype):
    
    with open(filename) as f:
        filedata = f.read().splitlines()
        
    filedata = [datatype(elem) for elem in filedata]
    
    return filedata


"""
  [DEBUG] Faz a impressão de valores.
  
  Entradas: objNames = lista de strings, representa os nomes dos objetos
                       a serem impressos
            objValues = lista de elementos, podendo ser de qualquer tipo,
                        são os valores dos objetos a serem impressos

  Saídas: n/a
"""
def DEBUG_PrintNamesAndValues(objNames, objValues):    
    
    for name, value in zip(objNames, objValues):
        print(f"{name}:")
        print(f"Size: {len(value)}")
        print(f"{value}")
        print("")


"""
  Abre todos os arquivos de votações de uma vez só, para poupar processamento.
  
  Entradas: n/a
  
  Saídas: n/a
"""
def openAllMotions():
    
    global votingFilesOpen
    global idVotacaoList
    global idPropList
    global jsonDataList
    
    print("""Opening vote files...\n""")
    print("""This may take a while\n""")
    
    jsonFiles = listFiles(f"{jsonFilesDir}", ".json")
    
    for jsonFile in jsonFiles:
        myList = getVotingIdentifiers(jsonFile)

        idVotacao = int(myList[0])
        idProp = int(myList[1])
        
        jsondata = literal_eval(str(openJSONFile(f"{jsonFilesDir}/"
                                                 f"{jsonFile}")))
        
        idVotacaoList.append(idVotacao)
        idPropList.append(idProp)
        jsonDataList.append(jsondata)
        
    votingFilesOpen = True
    
    removeInvalidIDs(jsonDataList)
    
    if (idVotacaoList and idPropList and jsonDataList):
        print("Opening: SUCCESS!\n")
    else:
        print("Opening: ERROR!\n")
        sys.exit(0)
