import tableFunctions as tF
import utilities as util


"""
  Função Main.
  
  Algoritmo:
  
    1) Abrir cada arquivo de votação
    2) Criar um CSV contendo as entradas correspondentes para as tabelas
       que contém dados sobre as votações ('Votacao', 'Votacao_has_Deputado' e
       'Votacao_has_Proposicao')
"""
def main():
    votingEntityList = ['Votacao', 'Votacao_has_Deputado',
                        'Votacao_has_Proposicao']
    
    for entity in votingEntityList:
        tF.processDataFile_MySQL(entity)


''' Execução da Main '''
main()
