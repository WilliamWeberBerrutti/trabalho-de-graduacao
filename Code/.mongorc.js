// Obtido de: 'https://stackoverflow.com/questions/14021605/mongodb-how-can-i-see-the-execution-time-for-the-aggregate-command'

function time(command) {
    const t1 = new Date();
    const result = command();
    const t2 = new Date();
    print("time: " + (t2 - t1) + "ms");
    return result; 
}
