LOAD DATA LOCAL INFILE 'Arquivos/proposicoes.csv'
INTO TABLE Proposicao_has_Deputado
CHARACTER SET 'utf8'
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(@col1,@col2,@col3,@col4,@col5,@col6,@col7,@col8,
@col9,@col10,@col11,@col12,@col13,@col14,@col15,@col16,
@col17,@col18,@col19,@col20,@col21,@col22,@col23,@col24,
@col25,@col26,@col27,@col28,@col29,@col30)

SET

Proposicao_idProposicao = @col1,
Deputado_idDeputado = (SELECT SUBSTRING_INDEX(@col20, '/', -1));

