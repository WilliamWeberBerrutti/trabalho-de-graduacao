LOAD DATA LOCAL INFILE 'Arquivos/votacoesProposicoes.csv'
INTO TABLE Votacao_has_Proposicao
CHARACTER SET 'utf8'
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(@col1,@col2,@col3,@col4)

SET

Votacao_idVotacao = @col1,
Votacao_codSessao = @col2,
Votacao_hora = @col3,
Proposicao_idProposicao = @col4;

