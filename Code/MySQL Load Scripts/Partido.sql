LOAD DATA LOCAL INFILE 'Arquivos/partidos.csv'
INTO TABLE Partido
CHARACTER SET 'utf8'
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(@col1,@col2,@col3,@col4)

SET

idPartido = @col1,
nome = @col2,
siglaPartido = @col3;

