LOAD DATA LOCAL INFILE 'Arquivos/frentesDeputados.csv'
INTO TABLE Frente_has_Deputado
CHARACTER SET 'utf8'
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(@col1,@col2,@col3,@col4,@col5,@col6,@col7,@col8,
 @col9,@col10,@col11,@col12,@col13,@col14)

SET

Frente_idFrente = @col1,
Deputado_idDeputado = @col4,
Titulo = @col12;

