LOAD DATA LOCAL INFILE 'Arquivos/deputadosPartidos.csv'
INTO TABLE Deputado_has_Partido
CHARACTER SET 'utf8'
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(@col1,@col2,@col3,@col4,@col5,@col6,@col7)

SET

Deputado_idDeputado = @col1,
Partido_idPartido = (SELECT SUBSTRING_INDEX(@col7, '/', -1));

