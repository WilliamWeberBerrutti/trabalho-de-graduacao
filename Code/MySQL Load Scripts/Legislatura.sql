LOAD DATA LOCAL INFILE 'Arquivos/legislaturas.csv'
INTO TABLE Legislatura
CHARACTER SET 'utf8'
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(@col1,@col2,@col3,@col4,@col5)

SET

idLegislatura = @col1,
dataInicio = @col3,
dataFim = @col4;

