LOAD DATA LOCAL INFILE 'Arquivos/votacoesDeputados.csv'
INTO TABLE Votacao_has_Deputado
CHARACTER SET 'utf8'
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(@col1,@col2,@col3,@col4,@col5)

SET

Votacao_idVotacao = @col1,
Votacao_codSessao = @col2,
Votacao_hora = @col3,
Deputado_idDeputado = @col4,
voto = @col5;

