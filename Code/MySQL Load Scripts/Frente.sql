LOAD DATA LOCAL INFILE 'Arquivos/frentes.csv'
INTO TABLE Frente
CHARACTER SET 'utf8'
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(@col1,@col2,@col3,@col4,@col5,@col6,@col7,@col8,
 @col9,@col10,@col11,@col12,@col13,@col14,@col15,@col16,
 @col17,@col18,@col19,@col20)

SET

idFrente = @col1,
titulo = @col3,
dataCriacao = @col4;

