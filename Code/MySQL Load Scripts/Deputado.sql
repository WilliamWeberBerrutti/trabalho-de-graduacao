LOAD DATA LOCAL INFILE 'Arquivos/deputados.csv'
INTO TABLE Deputado
CHARACTER SET 'utf8'
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(@col1,@col2,@col3,@col4,@col5,@col6,@col7,@col8,
 @col9,@col10,@col11,@col12,@col13)

SET

idDeputado = (SELECT SUBSTRING_INDEX(@col1, '/', -1)),
nome = @col5,
sexo = @col7,
siglaUF = @col12;

