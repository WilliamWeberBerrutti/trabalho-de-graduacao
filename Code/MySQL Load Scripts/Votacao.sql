LOAD DATA LOCAL INFILE 'Arquivos/votacoes.csv'
INTO TABLE Votacao
CHARACTER SET 'utf8'
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(@col1,@col2,@col3,@col4,@col5,@col6)

SET

idVotacao = @col1,
codSessao = @col2,
resumo = @col3,
objVotacao = @col4,
data = @col5,
hora = @col6;

