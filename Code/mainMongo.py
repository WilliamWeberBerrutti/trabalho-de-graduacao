import string
import sys
from timeit import default_timer

import tableFunctions as tF
import utilities as util


"""
  Função Main.
  
  Algoritmo:
  
    1) Conectar ao BD
    2) Abrir cada arquivo de dados dos Dados Abertos da Câmara, previamente
       baixados da aba ‘Arquivos’ do site ou da aba 'API RESTFul'
    3) Após a abertura de cada arquivo, inserir os dados relevantes no BD de
       documentos
    4) Para as Votações: usar o 'getVotes.py' para fazer requisições HTML GET
       para obter os arquivos, contendo  dados das votações das proposições
    5) Após obter os arquivos das votações, inseri-los no Mongo
"""
def main():
    
    ''' Conecta ao MongoDB '''
    db, client = util.connectMongoDB()
    
    # DEBUG
    ''' Deleta a base de dados para fins de teste '''
    '''
    print(f"\n ==== Deletando a base de dados ==== \n")
    
    client.drop_database(util.dataBaseName)
    '''
    
    ''' Obtém uma lista de arquivos com extensão .csv '''
    csvDataFiles = util.listFiles(util.csvFilesDir, ".csv")
    
    ''' Remove CSVs desnecessários para simplificar processamento '''
    csvDataFiles = util.removeCSVsForMongo(csvDataFiles)
    
    total_time = 0
    
    ''' Processa todas as coleções '''
    for csvfile in csvDataFiles:
        
        collectionName = str(csvfile).replace(".csv", "")
        
        print(f"\n\n ==== Processando Coleção: {collectionName} ==== \n")
        
        start_time = default_timer()
        
        ''' Cria uma coleção nova com o arquivo CSV e o processa '''
        collection = db[collectionName]
        
        dictList = tF.processDataFile_Mongo(collectionName, csvfile)
        
        ''' Insere os dados no MongoDB '''
        util.insertDocs(collection, dictList)
        
        elapsed = default_timer() - start_time
            
        print(f"Time: {round(elapsed, 8)} seconds")
        
        total_time += elapsed
    
    
    client.close()
    
    print(f"\n\n ==== Connection closed ==== \n")
    
    print(f"Time Elapsed: {round(total_time, 8)} seconds")
    print(f"Docs Inserted: {util.countTotal}")


''' Execução da Main '''
main()
