import json
import os
import shutil
import time

import requests
import xmltodict

import utilities as util


savedFile = "Prop"
votesDir = "Votacoes"
propList = []

propLogList = []


'''
  Tem que ser uma var global e inserida artificialmente
'''
idVotacao = 1


"""
  Obtém as votações de todas as Proposições já votadas no Plenário.
  
  Entradas: json_data = lista de strings, representa o arquivo JSON aberto
  
  Saídas: n/a
"""
def getProposition(json_data):
    
    len_json = len(json_data)

    for idx in range(len_json):
        year = json_data[idx]['ano']
        
        print(f" ==== Processando ano: {year} ==== \n")
        
        if (not os.path.exists(f"{votesDir}")):
            os.makedirs(f"{votesDir}")
        
        codProp = 0
        for elem in json_data[idx]["data"]["proposicoes"]["proposicao"]:
            codProp_prev = elem["codProposicao"]
            
            if (codProp_prev != codProp):
                idProp = codProp_prev
                initials, number, year = util.getActualPropositionIdentifiers(
                                                        elem["nomeProposicao"])
                getVoting(idProp, initials, number, year)
                
                codProp = codProp_prev
            
            
"""
  Obtém as votações de uma Proposição já votada no Plenário.
  
  Entradas: idProp = inteiro, é o ID de uma Proposição
            initials = string, 
            number = inteiro, é o nº da Proposição
            year = inteiro, é o ano em que a Proposição foi formulada
  
  Saídas: n/a
"""
def getVoting(idProp, initials, number, year):
    
    global idVotacao
    
    jsonFile = f"{votesDir}/{savedFile} {idVotacao} {idProp} {initials} " \
               f"{number} {year}.json"
    
    print(f"Processando '{jsonFile}' ...")
    
    if ([initials, number, year] not in propLogList):
    
        '''
          Checa se o arquivo já existe no diretório local, para não precisar
          fazer uma requisição HTML GET sem necessidade
        '''
        if (os.path.isfile(jsonFile)):
            print(f"O arquivo JÁ EXISTE! Indo para o próximo ...")
            
            idVotacao += 1
            
        else:
            xmlString = requests.get(f"https://www.camara.leg.br/SitCamaraWS/"
                                   f"Proposicoes.asmx/ObterVotacaoProposicao?"
                                 f"tipo={initials}&numero={number}&ano={year}")
            
            if (xmlString.status_code == 200):
                print(f"{jsonFile} foi ENCONTRADO! Salvando...")
                
                jsonString = json.dumps(xmltodict.parse(xmlString.text),
                                        indent=4, ensure_ascii=False)
                
                with open(jsonFile, 'w') as f:
                    f.write(str(jsonString))
                    
                idVotacao += 1
                
            else:
                print(f"O arquivo NÃO FOI ENCONTRADO! Indo para o próximo...")
            
        print("")
    else:
        print(f"A proposição {initials} {number} {year} já existe!"
              f"Indo para o próximo...")


"""
  Função Main
  
  Algoritmo:
  
  1) Abrir o arquivo 'allMotionsPerYear.json', que contém todas as
     proposições votadas em plenário 
  2) Obter os votos de cada proposição, salvando no nome do arquivo o
     idVotacao e idProposicao, para facilitar / simplificar processamento
"""
def main():
    
    util.motionsFile = util.openJSONFile("allMotionsPerYear.json")
    getProposition(util.motionsFile)


''' Execução da Main '''
main()
